Bem Vindo a minha proposta de solução do desafio de desenvolvimento para o processo seletivo do Vortex Unifor 2021.2.<br>
----------------------------------------------------------------------------------------------------------------------------------
**Nesse projeto foram utilizadas as seguintes tecnologias:**<br>
-Javascript ES6.<br>
-Node.js v14.17.1.<br>
-prompt-sync 4.2.0.<br>
-strip-ansi 5.2.0 <br>
**problema proposto**<br>
"o treinador Mafrashow desenvolveu um treino que tem como foco melhorar os passes<br>
rápidos e reposicionamentos do time. As instruções para esse treinamento são<br>
as seguintes:<br>
<br>
● Cada jogador deve dominar a bola por um único segundo e então deve<br>
passá-la para um jogador livre.<br>

● Depois de fazer o passe, o jogador deve começar a correr por X segundos<br>
para se reposicionar na quadra e não poderá receber nenhum passe até<br>
que termine.<br>
<br>
Desenvolva um programa que calcule o somatório de jogadores livres<br>
para receber um passe em cada segundo do jogo até o limite de tempo T<br>
estabelecido usando as regras do treinamento acima.<br>
respeite os seguintes limites para as variaveis:<br>
 * 0 < tempo total da jogada <= 24<br>
 * 2 <= número total de jogadores <= 5<br>
 * 0 < tempo de reposicionamento <= (número total de jogadores - 1)"<br>


**Método de solução:**<br>
para solucionar esse desafio foi desenvolvido um modelo matematico que soluciona o problema proposto,<br>
esse modelo é o seguinte:<br>
para Q = tempo total da jogada.<br>
para T = número total de jogadores.<br>
para X = tempo de reposicionamento.<br>
para S = o somatório de jogadores livres para receber um passe em cada segundo do jogo<br>
temos a seguinte formula,<br>
S = (Q-1)+(Q-2)+...+(Q-x)+...+(Q-x)<br>
onde essa série de somas possui T termos,<br>
**Exemplo:**<br>
T = 7; Q = 5; X = 3<br>
S = (5-1)+(5-2)+(5-3)+(5-3)+(5-3)+(5-3)+(5-3)<br>
S = 17<br>
