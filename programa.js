const prompt = require('prompt-sync')();


//função para resolver o problema
const modeloMat = () =>{
    console.log(`Bem Vindo, com os dados requisitados a seguir iremos calcular o somatorio somatório de jogadores livres
para receber um passe em cada segundo do jogo durante o treinamento proposto pelo treinador de basquete Mafrashow.

este treinamento consiste em duas regras simples, cada jogador deve dominar a bola por um único segundo e então deve
passá-la para um jogador livre, Depois de fazer o passe, o jogador deve começar a correr por X segundos para se reposicionar 
na quadra e não poderá receber nenhum passe até que termine.

precisamos que voce insira 3 variáveis para o nosso treino. 
O tempo total da jogada. Esse valor deve ser um número maior que 0 e menor ou igual a 24 (0 < T <= 24),
O número total de jogadores. Esse valor deve ser um número maior ou igual a 2 e menor ou igual a 5 (2 <= Q <= 5),
O tempo de reposicionamento dos jogadores após o passe. Esse valor deve ser um número maior que 0 e menor ou igual ao número de total de jogadores menos um (0 < X <= Q-1) 
`)
    T = prompt('Qual o valor para o tempo total da jogada? ');
    Q = prompt('Qual o valor para o número total de jogadores? ');
    X = prompt('Qual o valor para o tempo de reposicionamento dos jogadores após o passe? ');
//condicionais para os argumentos    
    let errorCount = 0;
    if (T <= 0 || T > 24 || isNaN(T) == true ){
        console.log("Valor para o tempo total da jogada inválido. Esse valor deve um número ser maior que 0 e menor ou igual a 24 (0 < T <= 24)");
        errorCount += 1;
    }if(Q < 2 || Q > 5 || isNaN(Q) == true ){
        console.log("Valor para o número total de jogadores inválido. Esse valor deve ser um número maior ou igual a 2 e menor ou igual a 5 (2 <= Q <= 5)");
        errorCount += 1;
    }if(X <= 0 || X > (Q-1) || isNaN(X) == true ){
        console.log(`Valor para o tempo de reposicionamento dos jogadores apos o passe inválido. Esse valor deve um número ser maior que 0 e menor ou igual ao número de total de jogadores menos um (0 < X <= Q-1)`
                    );
        errorCount += 1;
    }if(errorCount > 0){
        const resposta = prompt("Deseja iniciar novamente? caso queira, digite \"sim\" ")
        switch (resposta) {
            case 'sim':
                modeloMat()
                break;
            
            default:
                break;
        }
    }else{
//lógica de solução do problema proposto através de um modelo matemático
        let operador = 1
        let soma = 0
            while (T > 0) {
                if (operador != X ) {
                    soma += (Q - operador)
                    operador += 1;
                    T -= 1;
                }else if (operador == X){
                    soma += (Q-X)
                    T -=1;
                }
        }
        return console.log(soma)
    }
}


//chamada da função 
modeloMat();


/*
explicação da solução:
para solucionar esse desafio foi desenvolvido um modelo matemático que soluciona o problema proposto,
esse modelo é o seguinte:
para Q = tempo total da jogada.
para T = numero total de jogadores.
para X = tempo de reposicionamento.
para S = o somatório de jogadores livres para receber um passe em cada segundo do jogo
temos a seguinte formula,
S = (Q-1)+(Q-2)+...+(Q-x)+...+(Q-x)
onde essa serie de somas possui T termos,
Exemplo:
T = 7; Q = 5; X = 3
S = (5-1)+(5-2)+(5-3)+(5-3)+(5-3)+(5-3)+(5-3)
S = 17
*/

